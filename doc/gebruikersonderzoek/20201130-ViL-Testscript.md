**Project ‘ViL – Interview Gespreksleidraad**

**Gesprek met:**

**VLP/Purpose:** Gina & Sander

**Datum:**

**Onderwerp:** Gebruiksonderzoek voor het Virtueel Inkomstenloket

<table>
<tbody>
<tr class="odd">
<td><p><strong>Hoofdvragen onderzoek</strong></p>
<ul>
<li><p><em>In hoeverre is men in staat om de regelingen/toeslagen aan te vragen waar zij recht op hebben?</em></p>
<ul>
<li><p><em>In hoeverre begrijpt men de stappen van aanvraag?</em></p></li>
<li><p><em>In hoeverre begrijpt men de uitkomst en vervolgstappen?</em></p></li>
</ul></li>
<li><p><em>Wat zijn de knelpunten, afhaakmomenten, en belemmeringen?</em></p></li>
<li><p><em>Naar welke prototype gaat de voorkeur uit?</em></p></li>
</ul></td>
</tr>
</tbody>
</table>

Respondenten voeren de taken zoveel mogelijk zelfstandig uit en waar
mogelijk stemmen we scenario’s af op hun persoonlijke situatie. 

Terwijl de respondent de taken uitvoert, letten wij vooral op: 

-   ❯  **Verwachtingen: **​​Werkt alle functionaliteit zoals men
    verwacht? 

-   ❯  **Begrip:**​​ Is alle functionaliteit en inhoud helder en
    begrijpelijk? 

-   ❯  **Gedrag:**​​ Waar kijkt & klikt men? Welke elementen gebruikt
    men wel/niet? 

-   ❯  **Beleving: **​​Hoe ervaart en waardeert men het idee of
    product? 

**  
**

**Interview gespreksleidraad**

**Intro**

Vandaag wil ik het graag hebben over jouw ervaringen met het aanvragen
van hulp bij je inkomen in Nederland. Eerst wil ik je wat algemene
vragen stellen, daarna laat ik je een app zien.

**Voordat we naar het prototype gaan gebruiken, vertellen we de
respondent:**

-   We testen het prototype en niet jou. Jij kan dus niks fout doen. We
    testen een idee of product en willen vooral leren wat wel of niet
    werkt, en daar help je ons mee

-   Denk zoveel mogelijk hardop en voel je volledig vrij om te zeggen
    wat je van het product denkt of vindt

-   *Eventueel toestemming vragen om het op te nemen*

**Pre-test**

**Algemeen**

-   Kan je iets over jezelf vertellen? (gezinssituatie, wat doe je, waar
    woon je? Waar kom je vandaan?)

**Inburgeringstraject**

-   Hoe lang ben je al in Nederland?

-   Hoe ver ben je met het inburgeringstraject? Weet je hoe lang het nog
    duurt?

**Werk**

-   Wat voor werk doe je?

-   Wat voor contract heb je? (0-uren, tijdelijk, vast)?

**Geldzaken en hulp bij inkomen**

-   Hoe regel je nu je bankzaken/financiën? (*alleen of met hulp, via
    laptop/telefoon)*

-   Wat weet je van hulp bij inkomen in Nederland? Hoe ben je daar
    achter gekomen?

-   Heb jij hulp bij je inkomen? (*van vrienden, familie, instanties, of
    huurtoeslag/zorgtoeslag?)*  
    Van welke hulpmiddelen maak jij nu gebruik? (*bijv. huurtoeslag
    /zorgtoeslag/kinderbijstand)*

-   Wat moest je daarvoor doen?

-   Ervaring: wat was moeilijk en wat was makkelijk?

**Gebruik van tools**

-   We willen je zo een app laten gebruiken

-   Waarvoor gebruik je je telefoon nu allemaal? (*Facebook, Instagram,
    internetbankieren, betaallink; tikkie, contactloos betalen (NFC),
    nieuwsapp*)

-   Waarvoor gebruik je die app? Wat was moeilijk/makkelijk?

**Prototype 1 & 2**

<table>
<tbody>
<tr class="odd">
<td><p><em>Afhankelijk van pre-test:</em></p>
<ul>
<li><p><em>Stel je voor, je gaat naar de cursus over geld tijdens je inburgeringsperiode. Daar vertelt de trainer dat er in Nederland één plek is waar je alle hulp rondom inkomen en spullen kunt aanvragen. Je bent benieuwd of je ook ergens recht, je download de app op je telefoon, en gaat aan de slag. Dit is wat je ziet (opent app)</em></p></li>
<li><p><em>Stel je voor, je persoonlijk begeleider (VWN) vertelt je dat je op één plek alles omtrent inkomen kan regelen, samen openen jullie de app en gaat aan de slag om extra hulp omtrent je inkomen aan te vragen. Je bent benieuwd of je ook ergens recht op hebt en je gaat aan de slag.</em></p></li>
</ul></td>
</tr>
</tbody>
</table>

*We laten de verschillende prototypes aan de respondenten zien. We
wisselen om per respondent*

**Introductie app**

-   Met het Virtueel Inkomstenloket kun je op één plek verschillende
    regelingen en toeslagen aanvragen en ontvangen

**Welkomscherm: verhoog je inkomen**

-   In hoeverre begrijpt men wat men met de tool kan?

-   Wat verwacht men van de tool?

-   Willen mensen de video bekijken? Wat verwachten ze hier te zien?

<!-- -->

-   *Kan je in je eigen woorden uitleggen wat je hier kan doen?*

-   *Is deze tool relevant voor jou? Hoe weet je dat?*

**Selectiescherm: Ik doe de aanvraag alleen/met hulpverlener**

-   Waar kiest men voor en waarom?

-   Wat verwacht men van de verschillende opties?

<!-- -->

-   *Waarom kies je voor deze optie?*

**Inloggen met DigiD & gegevens ophalen**

-   Begrijpt men de stappen?

-   Lukt het om de gegevens op te halen?

-   In hoeverre begrijpt men hoe en waarom de gegevens worden verzameld?

-   Wat verwacht men van de uitslag?

Schermen: ‘Gelukt’ gegevens zijn opgehaald bij de
belastingdienst/rijksoverheid*:*

-   *Kan je in je eigen woorden uitleggen wat je zojuist hebt gedaan?*

-   *Waarvoor hebben ze jouw gegevens nodig?*

-   *Wat verwacht je onder uitslag?*

**Aanvraag versturen**

-   In hoeverre begrijpt men de uitslag?

-   In hoeverre wil men toegang extra uitleg/info over de IIT?

<!-- -->

-   *Wat is de uitslag?*

-   *Waar is dit resultaat op gebaseerd? Hoe weten ze dat jij hier recht
    op hebt?*

-   *Ben je bekend met de IIT?*

-   *Wat gebeurd er als je nu op ‘verstuur de aanvraag’ klikt?*

**We gaan het voor u regelen**

-   In hoeverre begrijpt men de vervolgstappen?

<!-- -->

-   *Wat gaat er nu gebeuren?*

**We houden u op de hoogte**

-   In hoeverre begrijpt men de vervolgstappen?

<!-- -->

-   *Wat gaat er nu gebeuren?*

-   *Kan je in je eigen woorden vertellen wat je zojuist hebt gedaan?*

-   *Klopt dit met wat je had verwacht aan het begin?*

-   *Heb je nog vragen?*

**Toegang tot hulp**

-   In hoeverre wil men toegang tot hulp?

-   Aan wie vraagt men hulp (vrienden familie, persoonlijk begeleider,
    instantie etc.)

-   In welke vorm zou men om hulp willen vragen?

-   In hoeverre wil men hier zelfstandig gebruik van kunnen maken?

<!-- -->

-   *Is het je opgevallen dat je om hulp kon vragen?*

-   *Aan wie zou jij hulp vragen?*

-   *Nu je de aanvraag hebt doorlopen; doe je dit liever alleen of met
    een hulpverlener?*

**Evaluatie**

-   Hoe vond je het gaan?

-   Als er iets verbeterd kan worden, wat moet dat dan wat jou betreft
    zijn? (bijv. taalgebruik)

-   Mis je nog dingen?

-   Heb je zelf nog vragen of opmerkingen of tips?

-   In hoeverre zou je dit gebruiken?

**Aan het einde leggen we het andere prototype voor**

-   Naar welk prototype gaat je voorkeur uit?

**Vragen Lisanne / communicatie**

> -        Als het Virtueel Inkomensloket er is, willen we inwoners
> bewegen ernaartoe te gaan en inkomensondersteuning aan te vragen:

-   Wat *motiveert* (intrinsiek en extrinsiek) jou om naar het loket te
    gaan? –

    -   Waarom zou je gebruik maken van deze app?

<!-- -->

-   Via *wie* of *welk kanaal* wil je horen over het Virtueel
    Inkomensloket?

    -   *Wanneer zou je dit aangeboden willen krijgen?*

    -   Door wie?

    -   Via wie of wat verwacht je hierover te horen? Waarom daar?
        *(gemeente, VWN, hulpverlener, budgetcoach, via social media,
        website van gemeente, reclame in het bushokje)*

-   Wat heb jij *nodig* om naar het loket te gaan?

    -   Wat heb je nodig om dit te kunnen gebruiken? (*bijv. hulp,
        telefoon, wifi, DigiD*?)

    -   Zelfstandig of samen?

**Vervolg**

-   Zijn er nog meer mensen in jouw netwerk waar we in de toekomst
    (december) contact mee mogen opnemen? (we hebben bijvoorbeeld nog
    geen contact opgenomen met Amer).
