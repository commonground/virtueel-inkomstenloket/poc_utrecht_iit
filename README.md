# Strategie Virtueel Inkomensloket

**Onze droom**

- Een simpel digitaal loket voor alle regelingen dat elke inwoner of hulpverlener kan gebruiken
- Je ziet niet alleen waar je recht op hebt, maar het wordt ook gelijk toegekend
- De regeleigenaren steunen op de gevalideerd data van de inwoner en de eigen regeling die in de regelchecker wordt ingevoerd en bijgehouden in het virtuele loket
- De inwoner kan een maandelijkse check up doen

**Aanpak**

Het ontwikkelteam is op dit moment bezig met de eerste regeling (de individuele Inkomenstoeslag) technisch werkend te krijgen in het virtueel inkomensloket. Aan de ene kant wordt er een front-end (voorkant van de website) gebouwd, die toegankelijk is voor alle inwoners en die de inwoner in staat stelt zijn eigen data op te halen uit landelijke databronnen. Aan de andere kant wordt een regelchecker gebouwd waarlangs de data van de inwoner wordt getoetst om te bepalen of de inwoner aan de regeling voldoet. Het Virtueel inkomensloket wordt radicaal open source ontwikkeld. Alles wat we bouwen wordt opengezet voor alles en iedereen. Dit betekent dat iedereen vrij is om het te gebruiken en om mee te bouwen.

**Het ontwikkelteam**

- Evelien van Geffen is opdrachtgever voor de gemeente Utrecht
- Vanuit Utrecht
  - Pieter in &#39;t Hout strategisch adviseur
  - IMP W&amp;I: Rob hendriks IPM W&amp;I
  - Domstad IT: UX-designers. Front-end design
  - Beleid W&amp;I: Roger van Loon (niet vast in ontwikkelteam)

- Belangrijkste partners
  - [ICTU](https://www.ictu.nl/): Product Owner Steven Gort.
  - Stichting Vaste Lasten Pakket: Projectleiders gebruiksonderzoek Applicatie (Sander Spinder en Gina Henselmans).
  - VNG: Architect API. Koppelingen om gegevens uit te wisselen (Gershon Jansen).
  - VNG strategie en ontwikkeling (Wouter Heijnen)
  - TWI aangesloten om een module te maken om beleidsregels straks in te kunnen vullen en door te vertalen naar codes in de applicatie.
  - Belastingdienst die nu de regelchecker om niet gaat ontwikkelen in onze POC;
  - Belastingdienst Toeslagen die het Virtueel Inkomstenloket promoot binnen de eigen organisatie (idem voor het UWV)
  - Het UWV die een vooronderzoek wil laten doen om de UWV-toeslag op te nemen in het VIL
  - Wigo4it (John Dieleman) die de data vanuit de POC in Socrates kan laten inlezen.
  - Gemeente Amersfoort
  - Alle leden van het ontwikkelteam leveren hun uren om niet. De samenwerking is op dit moment op basis van gezamenlijke belang en ambitie en passie.

Voor de bouw van het Virtueel inkomensloket wordt ingezet op 2 sporen. Spoor 1 is een oplossing voor de korte termijn. Hierbij wordt Iwize gebruikt. Dit is een software die gebruikers in staat stelt persoonlijke gegevens op een eenvoudige manier te verzamelen en te delen. VNG (innovatie) heeft Iwize een formele opdracht te geven voor het bouwen van een applicatie (de front-end). De voorkeur voor de lange termijn gaat echter uit om gebruik te maken van API&#39;s. API&#39;s hebben de voorkeur om dat dit common ground proof is en je ook de meest up to date data uit de juiste databron kunt halen. Beide sporen zijn hieronder in de figuur weer gegeven.

![quick recap](doc/Pictures/2-sporen-rails.png)

De Proof of Concept (PoC) voor de eerste regeling, de Individuele Inkomenstoeslag is naar verwachting in februari technisch klaar. We hebben dan bewezen dat het mogelijk is voor een inwoner om zelf zijn data op te halen via o.a. mijn overheid en mijn belastingdienst. We komen daarom voor de keuze te staan hoe we verder gaan. De volgende scenario&#39;s zijn mogelijk:

## Scenario 1: We nemen de IIT-regeling gelijk in productie en blijven zelf controleren en beschikken

We nemen de PoC zodra gereed in productie. We starten met een pilot waarin inwoners de IIT via het Virtueel inkomensloket kunnen aanvragen. Rekening houdend met inregelen van productie in huidige werkproces WenI is de verwachte startdatum pilot 1-07-2021 (voorlopige inschatting):

- 1A: Wigo4It kan de data van de inwoner inlezen in Socrates waardoor onze medewerkers de informatie kunnen inladen in de bestaande systemen, kunnen controleren en toekennen.

- 1B: Het in productie nemen van de PoC inclusief beheer en hosting bij gemeente Utrecht (open zaak)

### Scenario 1X: We nemen de IIT-regeling in productie en gaan het controleren en beschikken digitaliseren

Dit is een vervolg op scenario 1 als dit goed loopt. In deze variant wordt digitaal automatisch een besluit genomen wanneer de inwoner volgens de regelchecker in aanmerking komt voor de IIT.

### Scenario 1Y: Nadat 1X succesvol is, voegen we regelingen toe aan het loket.

Dit is een vervolg op scenario 1X. Dit kunnen regelingen van W&amp;I zijn of van bijvoorbeeld UWV en Voedselbank. Dit scenario moet nog wel verder worden uitgewerkt als de hosting bij de gemeente Utrecht gaat plaatsvinden met niet gemeentelijke regelingen (nog niet onderzocht).

## Scenario 2: We gaan &quot;pilots&quot; met meer regelingen

Zodra de PoC met de eerste regeling gereed is gaan we door met toevoegen van minimaal vier regelingen in 2021. Zodra vijf regelingen in de PoC gereed zijn nemen we de applicatie in productie. We verwachten dat eind 2021 vijf regelingen in het Virtueel Inkomensloket technisch klaar zijn. We denken nu aan de volgende regelingen:

1. Regeling tegemoetkoming zorgkosten
2. UWV-toeslag
3. Regeling gekozen door Amersfoort (waarschijnlijk voedselbank)
4. NTB

# Klantreis
## Huidige klantreis Bijlage 1

![](doc/Pictures/klantreis-VIL-oud.png)

## Nieuwe Klantreis door gebruik van Virtueel Inkomensloket

![](doc/Pictures/klantreis-VIL-nieuw.png)

# Spoor 1

Bevat 3 experimenten van het Virtueel Inkomensloket

## Experiment I
Versimpelde aanvraag vanuit beleid en voor de doelgroep


- IIT regel is versimpeld voor makkelijke aanvraag
- Standaard gegevensdefinitie IIT (GBI en Financieel Paspoort)
- Max 4 simpele vragen zijn opgesteld die een eerste indicatie geven op het recht en de hoogte van de IIT
- Validatie bij de doelgroep leidt tot een uitgewerkt UX design (t/m opgehaalde data)

> ## Design Prototype
>
> - [versie 0.1](https://www.figma.com/file/jHAKDhY6XogAADSZjaxefP/Mobiel?node-id=5%3A0) - Demo 25 Nov 2020
> - [versie 0.2](https://www.figma.com/file/jHAKDhY6XogAADSZjaxefP/Mobiel?node-id=784%3A2) - Demo 13 Jan 2021
> - [versie 0.3](https://www.figma.com/proto/jHAKDhY6XogAADSZjaxefP/Mobiel?node-id=1542%3A4563&scaling=min-zoom) - Demo 2 Mar 2021
> - [versie 0.4](https://www.figma.com/proto/jHAKDhY6XogAADSZjaxefP/Mobiel?node-id=2388%3A11452&scaling=scale-down&page-id=2388%3A11113) - Demo tbd
> - [versie 0.5](https://www.figma.com/proto/jHAKDhY6XogAADSZjaxefP/Mobiel?node-id=2722%3A457&scaling=scale-down&page-id=2722%3A0) - Demo tbd
>
> ### Gebruikersinzichten
>
> - [Inzichten versie 0.1](https://miro.com/app/board/o9J_lal1hJg=/)
> - [Inzichten versie 0.2](https://miro.com/app/board/o9J_lWo9OV4=/)
> - [Inzichten versie 0.3](https://miro.com/app/board/o9J_lPDDGgo=/)
> - [Inzichten versie 0.4](https://miro.com/app/board/o9J_lMqKktM=/)
> 
> ### Kijk mee met de laatste designs
>
> In deze [Figma](https://www.figma.com/file/jHAKDhY6XogAADSZjaxefP/Mobiel?node-id=0%3A1) delen we de verschillende versies van de ontwerpen. De ‘page’ met het hoogste nummer is de laatste versie. Per versie is een changelog beschikbaar die laat zien wat is aangepast sinds de vorige versie. In elke versie staan geplaatste comments en antwoorden die betrekking hebben op die versie.


## Experiment II
De versimpelde aanvraag in een werkend prototype

- In een werkend prototype maken we gebruik van de 4 simpele vragen (indicatie op het recht)
- Inwoner kan eigen data ophalen mbv I-wize die relevant zijn voor IIT, in een werkend prototype
- In het prototype maken we gebruik van de standaard gegevens definitie

## Experiment III
De inwoner kan bepalen of zij/hij daadwerkelijk recht heeft op de IIT

- Regels voor het bepalen van recht op IIT worden ingevoerd in regelchecker van TWI
- Opgehaalde data wordt langs regelchecker gehaald
- Data en aanvraag wordt naar de validator (W&I back office) gestuurd