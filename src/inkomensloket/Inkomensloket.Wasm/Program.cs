using alef_graphql_client;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Inkomensloket.Wasm
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");

            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
            builder.Services.AddHttpClient(
                "AlefGraphQlClientClient",
                c => c.BaseAddress = new Uri("http://localhost:5080/graphql"));
            builder.Services.AddAlefGraphQlClientClient();
            await builder.Build().RunAsync();
        }
    }
}
