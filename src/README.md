# Virtual Inkomensloket (*TECH*)

Dit document bevat een beschrijving van de gekozen technologie in het prototype en hoe deze geconfigureerd kan worden.

## Front End

* De front-end is ontworpen met behulp van het NL Design System.
* De runtime voor de front end is web assembly voor C# in NET 5.0
  * Web assembly is een enabler voor privacy by design
  * Biedt de mogelijkheid tot dezelfde backend C# logica in de browser te draaien.
  * De regelingen en beslispaden kunnen berekend worden zonder gegevens te versturen over internet
  * De burger verzend pas gegevens van de front-end naar de back-end (via API) als deze de proefberekening aanvaard en als aanvraag wil indienen
* front-end containerization
  * De web assembly bevat uitsluitend statische content
  * De content wordt geserveerd vanaf een standaard NGINX server
  * De dockerfile 

### Interactie

```mermaid
sequenceDiagram
    loop rechtsbepaling
        Burger->>Browser: Regeling interactie
        activate WASM
        Browser-->>WASM: Bereken recht
        WASM->>Browser: Toon recht
    end
    deactivate WASM
    Burger-->>Browser: Dien aanvraag in
    Browser-->>WASM: Dien aanvraag in
    WASM-->>API: Dien aanvraag in
```

## Running demonstrator

Op dit moment is er nog geen werkende demo. De UX user tests zijn nog niet afgerond voor de schermen.
De infrastructuur is wel opgezet om de WASM website te hosten in een docker container via NGINX.

### Requirements

Op dit moment is er nog geen docker registry met prebuild images. De registry moet nog aangemaakt 
worden op Gitlab. Voor nu zal de docker image zelf gebuild moeten worden via bijgeleverde dockerfile.

De volgende dependencies moeten ge�nstalleerd worden op je werkstation.

* Docker (*Desktop on Win*)
* DOTNET (SDK 5.0.100) https://dotnet.microsoft.com/download/dotnet/5.0
* HELM v3 https://helm.sh/docs/intro/install/

### Build & Run

#### Docker

vanuit een prompt (root van deze repo)

```bash
$ cd src/inkomensloket
$ docker build -t inkomensloket-wasm .
$ docker run -p 8080:80 inkomensloket-wasm
```

```
/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
/docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
/docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
10-listen-on-ipv6-by-default.sh: Getting the checksum of /etc/nginx/conf.d/default.conf
10-listen-on-ipv6-by-default.sh: Enabled listen on IPv6 in /etc/nginx/conf.d/default.conf
/docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
/docker-entrypoint.sh: Configuration complete; ready for start up
```

Browse naar: http://localhost:8080

#### Kubernetes / HELM Op **Windows Docker Desktop**

Voer eerst docker build uit aangegeven in de vorige stap. Nadat de docker image op je lokale station 
is gebuild kun je de chart uitvoeren vanuit de root path van de repo.
Zorg ervoor dat kubernetes enabled is op de docker desktop. Om dit te betrachten kun je het beste 
even de manual volgen: https://docs.docker.com/docker-for-windows/

```bash
$ helm install inkomensloket-wasm ./charts/inkomensloket-wasm
NAME: inkomensloket-wasm
LAST DEPLOYED: Mon Nov 30 23:02:26 2020
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
```

Dit start de pod op (default 1 replica), welke de WASM web site serveert vanuit NGINX.
Om de status te controleren kun je het volgende commando uitvoeren:

```bash
$ kubectl get all --selector app=inkomensloket-wasm
NAME                                                 READY   STATUS    RESTARTS   AGE
pod/inkomensloket-wasm-deployment-768b6449f6-jm2xw   1/1     Running   0          35s

NAME                                 TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
service/inkomensloket-wasm-service   ClusterIP   10.109.66.197   <none>        5089/TCP   35s

NAME                                            READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/inkomensloket-wasm-deployment   1/1     1            1           35s

NAME                                                       DESIRED   CURRENT   READY   AGE
replicaset.apps/inkomensloket-wasm-deployment-768b6449f6   1         1         1       35s
```

Als alles in de running state is kun je alvorens de ingress geconfigureerd te hebben de web site
benaderen door de port van het clusterIP te forwarden.

```
$ kubectl port-forward service/inkomensloket-wasm-service 5089:5089
Forwarding from 127.0.0.1:5089 -> 80
Forwarding from [::1]:5089 -> 80
```

Browse naar http://localhost:5089

Deinstalleren van de app kan door het volgende commmando uit te voeren:

```
$ helm uninstall inkomensloket-wasm
release "inkomensloket-wasm" uninstalled
```

#### Kubernetes / HELM Op **Ubuntu met MicroK8s**

Op Ubuntu bevat de docker engine geen kubernetes. Voor lokale ontwikkeldoeleinden kun je gebruik
maken van MicroK8s. Meer info hier: https://microk8s.io/

##### Microk8s installeren met **snap**

Om Microk8s te installeren en geschikt te maken voor deployment via HELM installeren we naar kubernetes
ook HELM v3. Deze versie van HELM behoeft geen Tiller en maakt de installatie eenvoudiger.

Installeer de componenten middel:

```bash
$ sudo snap install microk8s --classic
$ microk8s enable helm3
```

Controleer of helm3 is geinstalleerd:

```bash
$ microk8s helm3 version
version.BuildInfo{Version:"v3.0.2", GitCommit:"19e47ee3283ae98139d98460de796c1be1e3975f", GitTreeState:"clean", GoVersion:"go1.13.5"}
```

Op windows resolved docker desktop kubernetes implementatie automatisch naar de local docker registry.
Echter op microk8s moeten we de values aanpassen in de chart values.yaml om de lokale docker image te
gebruiken.

Enable de local docker registry (dit hoeft je maar eenmalig te doen).

```yaml
$ microk8s enable registry
```

in /charts/inkomensloket-wasm/values.yaml zet de container image naar localhost:32000

```yaml
  image: localhost:32000/inkomensloket-wasm
```

Ga naar de /src/inkomensloket folder. Bouw en push de docker image

```batch
$ sudo docker build -t localhost:32000/inkomensloket-wasm .
$ sudo docker push localhost:32000/inkomensloket-wasm
```

Ga terug naar de root van de repo en installeer nu de helm chart

```bash
$ microk8s helm3 install inkomensloket-wasm ./charts/inkomensloket-wasm
LAST DEPLOYED: Tue Dec  1 20:54:08 2020
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
```

Controller of de pod werkt:

```bash
$ sudo microk8s kubectl get all --selector app=inkomensloket-wasm
NAME                                                 READY   STATUS    RESTARTS   AGE
pod/inkomensloket-wasm-deployment-694f6c9786-c9hbn   1/1     Running   0          33s

NAME                                 TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
service/inkomensloket-wasm-service   ClusterIP   10.152.183.215   <none>        5089/TCP   33s

NAME                                            READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/inkomensloket-wasm-deployment   1/1     1            1           33s

NAME                                                       DESIRED   CURRENT   READY   AGE
replicaset.apps/inkomensloket-wasm-deployment-694f6c9786   1         1         1       33s
```

Als alles in de running state is kun je alvorens de ingress geconfigureerd te hebben de web site
benaderen door de port van het clusterIP te forwarden.

```
$ microk8s kubectl port-forward service/inkomensloket-wasm-service 5089:5089
Forwarding from 127.0.0.1:5089 -> 80
Forwarding from [::1]:5089 -> 80
```

Browse naar http://localhost:5089

Deinstalleren van de app kan door het volgende commmando uit te voeren:

```
$ microk8s helm3 uninstall inkomensloket-wasm
release "inkomensloket-wasm" uninstalled
```

## Backend

De backend bevat een GraphQL gateway. In de graph kunnen verscheidene regelingen geplaatst worden voor interactie in de front end van het inkomensloket.

### Installatie

De backend kan geinstalleerd worden met docker-compose, momenteel serveert deze een stub vanuit de IIT XSD alef definitie.

```
cd ./inkomensloket-backend/alef-graphql-api
docker-compose up
```
Een interactieve API User Interface is nu beschikbaar voor de front end developer.

![graph definition](./docs/images/graphql-definition.png)
![graph definition](./docs/images/graphql-query.png)

Browse naar: http://localhost:5080/graphql

### Query

Hier een query voorbeeld van de IIT

```graphql
{
  request(invoer:{
    alleenstaand: true
    aowLeeftijdBehaald: false
    inkomenPerMaand: 1100
    ouderDan21: true
    thuiswonendeKinderen: false
    vermogen: 12000
    woonplaats: "Utrecht"
  })
  {
    rechtBeschrijving
    uitTeKerenToeslagBedrag
    uitTeKerenToeslagBedragSpecified
  }
}
```
